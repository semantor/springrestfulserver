package com.gitlab.semantor.restful.service;

import com.gitlab.semantor.restful.dao.BankAccountDAO;
import com.gitlab.semantor.restful.dao.ClientDAO;
import com.gitlab.semantor.restful.dao.PaymentDAO;
import com.gitlab.semantor.restful.entity.BankAccount;
import com.gitlab.semantor.restful.entity.Client;
import com.gitlab.semantor.restful.entity.Payment;
import com.gitlab.semantor.restful.exceptions.AlreadyExistException;
import com.gitlab.semantor.restful.exceptions.DatabaseException;
import com.gitlab.semantor.restful.exceptions.EntityNotFoundException;
import lombok.AccessLevel;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@Scope
public class ClientService {

    @Getter(AccessLevel.PROTECTED)
    private final ClientDAO clientDAO;
    @Getter(AccessLevel.PROTECTED)
    private final BankAccountDAO bankAccountDAO;
    @Getter(AccessLevel.PROTECTED)
    private final PaymentDAO paymentDAO;

    @Autowired
    public ClientService(ClientDAO clientDAO, BankAccountDAO bankAccountDAO, PaymentDAO paymentDAO) {
        this.clientDAO = clientDAO;
        this.bankAccountDAO = bankAccountDAO;
        this.paymentDAO = paymentDAO;
    }

    public List<Client> getAllClient() {
        return clientDAO.getAll();
    }

    public Client getClientByID(Integer valueOf) {
        Optional<Client> receivedClient = clientDAO.get(valueOf);
        if (receivedClient.isPresent()) return receivedClient.get();
        else throw new EntityNotFoundException();
    }

    public List<BankAccount> getBankAccountByClient(Integer valueOf) {
        return bankAccountDAO.getClientBankAccounts(valueOf);
    }

    public List<Payment> getClientPayments(Integer valueOf) {
        return paymentDAO.getClientPayments(valueOf);
    }

    public Client createNewClient(Client newClient) {
        List<Client> allClient = getAllClient();
        for (Client c : allClient) {
            if (c.equals(newClient)) throw new AlreadyExistException();
        }
        Optional<Client> receivedClient = clientDAO.add(newClient);
        if (receivedClient.isEmpty()) throw new DatabaseException();
        return receivedClient.get();
    }

    public Client updateClientByID(Integer valueOf, Client newClient) {
        if (clientDAO.get(valueOf).isEmpty()) throw new EntityNotFoundException();
        Optional<Client> client = clientDAO.update(valueOf, newClient);
        if (client.isEmpty()) throw new DatabaseException();
        return client.get();
    }

    public Client deleteClientByID(Integer id) {
        if (id < 1) throw new IllegalArgumentException();
        Optional<Client> receivedClient = clientDAO.delete(id);
        if (receivedClient.isEmpty()) throw new EntityNotFoundException();
        return receivedClient.get();
    }

}
