package com.gitlab.semantor.restful.service;

import com.gitlab.semantor.restful.dao.BankAccountDAO;
import com.gitlab.semantor.restful.dao.ClientDAO;
import com.gitlab.semantor.restful.dao.PaymentDAO;
import com.gitlab.semantor.restful.entity.BankAccount;
import com.gitlab.semantor.restful.entity.Client;
import com.gitlab.semantor.restful.entity.Payment;
import com.gitlab.semantor.restful.exceptions.DatabaseException;
import com.gitlab.semantor.restful.exceptions.EntityNotFoundException;
import lombok.AccessLevel;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@Scope
public class BankAccountService {

    @Getter(AccessLevel.PROTECTED)
    private final BankAccountDAO bankAccountDAO;
    @Getter(AccessLevel.PROTECTED)
    private final PaymentDAO paymentDAO;
    @Getter(AccessLevel.PROTECTED)
    private final ClientDAO clientDAO;

    @Autowired
    public BankAccountService(BankAccountDAO bankAccountDAO, PaymentDAO paymentDAO, ClientDAO clientDAO) {
        this.bankAccountDAO = bankAccountDAO;
        this.paymentDAO = paymentDAO;
        this.clientDAO = clientDAO;
    }

    public List<BankAccount> getAllBankAccounts() {
        return bankAccountDAO.getAll();
    }

    public BankAccount getBankAccountByID(int valueOf) {
        Optional<BankAccount> bankAccount = bankAccountDAO.get(valueOf);
        if (bankAccount.isEmpty()) throw new EntityNotFoundException();
        return bankAccount.get();
    }

    public List<Payment> getPaymentByBankAccount(int valueOf) {
        return paymentDAO.getBankAccountPayments(valueOf);
    }

    public BankAccount createNewBankAccount(int clientID) {
        Optional<Client> client = clientDAO.get(clientID);
        if (client.isEmpty()) throw new EntityNotFoundException();
        BankAccount bankAccount = new BankAccount(generateNewBankAccountNumber(), 0, client.get());
        Optional<BankAccount> newBankAccount = bankAccountDAO.add(bankAccount);
        if (newBankAccount.isEmpty()) throw new DatabaseException();
        return newBankAccount.get();

    }

    public BankAccount updateBankAccount(Integer valueOf, BankAccount newBankAccount) {
        if (bankAccountDAO.get(valueOf).isEmpty()) throw new EntityNotFoundException();
        Optional<BankAccount> receivedBA = bankAccountDAO.update(valueOf, newBankAccount);
        if (receivedBA.isEmpty()) throw new DatabaseException();
        return receivedBA.get();
    }

    public BankAccount deleteBankAccount(Integer id) {
        if (id < 1) throw new IllegalArgumentException();
        Optional<BankAccount> bankAccount = bankAccountDAO.delete(id);
        if (bankAccount.isPresent()) return bankAccount.get();
        else throw new IllegalArgumentException();
    }

    long generateNewBankAccountNumber() {
        return 1000_0000_0000_0000L + Math.round(Math.random() * 9000_0000_0000_0000L);
    }


}
