package com.gitlab.semantor.restful.dao;

import com.gitlab.semantor.restful.entity.BankAccount;
import com.gitlab.semantor.restful.entity.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Transactional
public class BankAccountDAOImpl implements BankAccountDAO {

    private SessionFactory sessionFactory;

    public BankAccountDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<BankAccount> add(BankAccount bankAccount) {
        Session session = sessionFactory.getCurrentSession();
        Integer id = (Integer) session.save(bankAccount);
        return get(id);
    }

    @Override
    public List<BankAccount> getAll() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<BankAccount> query = builder.createQuery(BankAccount.class);
        Root<BankAccount> root = query.from(BankAccount.class);

        query.select(root);
        List<BankAccount> bankAccounts =
                session.createQuery(query).getResultList();
        session.flush();
        return bankAccounts;
    }

    @Override
    public Optional<BankAccount> get(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        BankAccount result = (BankAccount) session.get(BankAccount.class, id);
        return Optional.ofNullable(result);
    }

    @Override
    public Optional<BankAccount> update(Integer id, BankAccount bankAccount) {
        Session session = sessionFactory.getCurrentSession();
        session.update(bankAccount);
        return get(id);
    }

    @Override
    public Optional<BankAccount> delete(Integer id) {
        Optional<BankAccount> bankAccount;
        Session session = sessionFactory.getCurrentSession();
        bankAccount = get(id);
        bankAccount.ifPresent(session::delete);
        return bankAccount;
    }

    public List<BankAccount> getClientBankAccounts(int id) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<BankAccount> paymentCriteriaQuery = builder.createQuery(BankAccount.class);
        Root<BankAccount> root = paymentCriteriaQuery.from(BankAccount.class);
        Join<BankAccount, Client> join = root.join("client");

        paymentCriteriaQuery.select(root).where(builder.equal(root.get("client"), id));
        return session.createQuery(paymentCriteriaQuery).getResultList();
    }

}
