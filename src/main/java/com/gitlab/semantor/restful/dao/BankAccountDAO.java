package com.gitlab.semantor.restful.dao;

import com.gitlab.semantor.restful.entity.BankAccount;

import java.util.List;

public interface BankAccountDAO extends DAO<Integer, BankAccount> {
    List<BankAccount> getClientBankAccounts(int id);
}
