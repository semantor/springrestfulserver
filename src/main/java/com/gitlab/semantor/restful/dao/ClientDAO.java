package com.gitlab.semantor.restful.dao;

import com.gitlab.semantor.restful.entity.Client;

public interface ClientDAO extends DAO<Integer, Client> {
}
