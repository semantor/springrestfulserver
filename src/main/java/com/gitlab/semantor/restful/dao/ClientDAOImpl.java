package com.gitlab.semantor.restful.dao;

import com.gitlab.semantor.restful.entity.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Transactional
public class ClientDAOImpl implements ClientDAO {


    private SessionFactory sessionFactory;

    public ClientDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<Client> add(Client newClient) {
        Session session = sessionFactory.getCurrentSession();
        Integer id = (Integer) session.save(newClient);
        return get(id);
    }


    @Override
    public List<Client> getAll() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Client> query = builder.createQuery(Client.class);
        Root<Client> root = query.from(Client.class);

        query.select(root);
        List<Client> client =
                session.createQuery(query).getResultList();
        session.flush();
        return client;
    }

    @Override
    public Optional<Client> get(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        Optional<Client> client = Optional.ofNullable(session.get(Client.class, id));
        return client;
    }

    @Override
    public Optional<Client> update(Integer id, Client client) {
        Session session = sessionFactory.getCurrentSession();
        session.update(client);
        return get(id);
    }

    @Override
    public Optional<Client> delete(Integer id) {
        Optional<Client> client;
        Session session = sessionFactory.getCurrentSession();
        client = get(id);
        client.ifPresent(session::delete);
        return client;
    }

}
