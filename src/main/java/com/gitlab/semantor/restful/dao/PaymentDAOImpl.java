package com.gitlab.semantor.restful.dao;

import com.gitlab.semantor.restful.entity.BankAccount;
import com.gitlab.semantor.restful.entity.Client;
import com.gitlab.semantor.restful.entity.Payment;
import com.gitlab.semantor.restful.entity.Status;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import java.util.List;
import java.util.Optional;

@Transactional
public class PaymentDAOImpl implements PaymentDAO {

    private SessionFactory sessionFactory;

    public PaymentDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<Payment> add(Payment payment) {
        Session session = sessionFactory.getCurrentSession();
        Integer id = (Integer) session.save(payment);
        return get(id);
    }

    @Override
    public List<Payment> getAll() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Payment> paymentCriteriaQuery = builder.createQuery(Payment.class);
        Root<Payment> paymentRoot = paymentCriteriaQuery.from(Payment.class);

        paymentCriteriaQuery.select(paymentRoot);
        List<Payment> result = session.createQuery(paymentCriteriaQuery).getResultList();
        return result;
    }

    @Override
    public Optional<Payment> get(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        Optional<Payment> payment = Optional.ofNullable(session.get(Payment.class, id));
        return payment;
    }

    public Optional<Payment> setStatus(int id, Status status) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaUpdate<Payment> update = builder.createCriteriaUpdate(Payment.class);
        Root<Payment> paymentRoot = update.from(Payment.class);

        update.set("status", status.getStatus()).where(builder.equal(paymentRoot.get("id"), id));
        session.createQuery(update).executeUpdate();
        return get(id);
    }

    public List<Payment> getClientPayments(int clientID) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Payment> query = builder.createQuery(Payment.class);
        Root<Payment> root = query.from(Payment.class);
        Join<Payment, BankAccount> bankAccountJoin = root.join("bankAccount");
        Join<BankAccount, Client> clientJoin = bankAccountJoin.join("client");

        query.select(root).where(builder.equal(clientJoin.get("id"), clientID));
        return session.createQuery(query).getResultList();
    }

    public List<Payment> getBankAccountPayments(int bankAccountID) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Payment> query = builder.createQuery(Payment.class);
        Root<Payment> root = query.from(Payment.class);

        query.select(root).where(builder.equal(root.get("bankAccount"), bankAccountID));

        return session.createQuery(query).getResultList();
    }

    @Override
    public Optional<Payment> update(Integer id, Payment payment) {
        Session session = sessionFactory.getCurrentSession();
        session.update(payment);
        return get(id);
    }

    @Override
    public Optional<Payment> delete(Integer id) {
        Optional<Payment> payment;
        Session session = sessionFactory.getCurrentSession();
        payment = get(id);
        payment.ifPresent(session::delete);
        return payment;
    }

}
