package com.gitlab.semantor.restful.dao;

import java.util.List;
import java.util.Optional;

public interface DAO<K, T> {
    Optional<T> get(K k);

    List<T> getAll();

    Optional<T> add(T t);

    Optional<T> update(K k, T t);

    Optional<T> delete(K k);
}
