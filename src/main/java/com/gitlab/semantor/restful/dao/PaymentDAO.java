package com.gitlab.semantor.restful.dao;

import com.gitlab.semantor.restful.entity.Payment;
import com.gitlab.semantor.restful.entity.Status;

import java.util.List;
import java.util.Optional;

public interface PaymentDAO extends DAO<Integer, Payment> {
    Optional<Payment> setStatus(int id, Status status);

    List<Payment> getClientPayments(int clientID);

    List<Payment> getBankAccountPayments(int bankAccountID);
}
