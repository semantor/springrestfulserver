package com.gitlab.semantor.restful.controllers;

import com.gitlab.semantor.restful.entity.Payment;
import com.gitlab.semantor.restful.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
public class PaymentController {

    @Autowired
    private PaymentService paymentService;


    @GetMapping("/payment")
    public List getAllPayments() {
        return paymentService.getAllPayments();
    }

    @GetMapping(value = "/payment/{id}")
    public Payment getAllPaymentByBankAccount(@PathVariable("id") int id) {
        return paymentService.getPaymentByID(id);
    }

    @PostMapping("/payment")
    public Payment createNewPayment(@RequestBody Payment payment) {
        return paymentService.createNewPayment(payment);
    }

    @DeleteMapping(value = "/payment/{id}")
    public ResponseEntity cancelPayment(@PathVariable("id") int id, HttpServletResponse response) {
        paymentService.cancelPayment(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);

    }
}
