package com.gitlab.semantor.restful.controllers;


import com.gitlab.semantor.restful.entity.BankAccount;
import com.gitlab.semantor.restful.entity.Payment;
import com.gitlab.semantor.restful.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
public class BankAccountController {

    @Autowired
    private BankAccountService bankAccountService;


    @GetMapping("/bankaccount")
    public List getAllBankAccounts() {
        return bankAccountService.getAllBankAccounts();
    }

    @GetMapping(value = "/bankaccount/{id}")
    public BankAccount getBankAccountByID(@PathVariable("id") int id) {
        return bankAccountService.getBankAccountByID(id);
    }

    @GetMapping(value = "/bankaccount/{id}/payments")
    public List<Payment> getPaymentByBankAccount(@PathVariable("id") int id) {
        return bankAccountService.getPaymentByBankAccount(id);
    }

    @PostMapping(value = "/bankaccount")
    public BankAccount createNewBankAccount(int id) {
        return bankAccountService.createNewBankAccount(id);
    }

    @PostMapping(value = "/bankaccount/{id}")
    public BankAccount updateBankAccount(@PathVariable("id") int id, @RequestBody BankAccount bankAccount) {
        return bankAccountService.updateBankAccount(id, bankAccount);
    }

    @DeleteMapping(value = "/bankaccount/{id}")
    public ResponseEntity deleteBankAccountByID(@PathVariable("id") int id, HttpServletResponse response) {
        bankAccountService.deleteBankAccount(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

}
