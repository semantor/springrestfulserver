package com.gitlab.semantor.restful.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "payments")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payment implements Serializable {
    public static final Payment defaultPayment = new Payment(0, new BankAccount(0, 0, null));

    @Id
    @Column(name = "payment_id")
    @EqualsAndHashCode.Exclude
    @Setter(AccessLevel.NONE)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    private int id;
    @OneToOne
    @JoinColumn(name = "bank_account_id")
    @ToString.Exclude
    @JsonProperty
    private BankAccount bankAccount;
    @Column(name = "price")
    @JsonProperty
    private int price;
    @Column(name = "date")
    @JsonProperty
    private Date date;
    @Column(name = "status")
    @JsonProperty
    private String status;

    public Payment(int price, BankAccount bankAccount) {
        this.price = price;
        this.bankAccount = bankAccount;
    }

    public void setStatus(Status status) {
        this.status = status.getStatus();
    }
}
