package com.gitlab.semantor.restful.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "bank_accounts")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BankAccount implements Serializable {
    public static BankAccount defaultBankAccount = new BankAccount(0,0,null);
    @Id
    @Column(name = "bank_account_id")
    @EqualsAndHashCode.Exclude
    @Setter(AccessLevel.NONE)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty
    private int id;
    @Setter(AccessLevel.NONE)
    @Column(name = "bank_account_number")
    @JsonProperty("bank_account_number")
    private long bankAccountNumber;
    @Column(name = "balance")
    @JsonProperty
    private int balance;
    @ManyToOne
    @JoinColumn(name = "client_id")
    @JsonProperty
    @EqualsAndHashCode.Exclude
    private Client client;

    public BankAccount(long bankAccountNumber, int balance, Client client) {
        this.bankAccountNumber = bankAccountNumber;
        this.balance = balance;
        this.client = client;
    }

    public BankAccount(long bankAccountNumber, int balance) {
        this.bankAccountNumber = bankAccountNumber;
        this.balance = balance;
    }

}
