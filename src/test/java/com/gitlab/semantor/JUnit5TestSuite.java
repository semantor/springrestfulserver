package com.gitlab.semantor;

import com.gitlab.semantor.restful.dao.BankAccountDAOImplTest;
import com.gitlab.semantor.restful.dao.ClientDAOImplTest;
import com.gitlab.semantor.restful.dao.PaymentDAOImplTest;
import com.gitlab.semantor.restful.service.BankAccountServiceTest;
import com.gitlab.semantor.restful.service.ClientServiceTest;
import com.gitlab.semantor.restful.service.PaymentServiceTest;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectClasses({BankAccountDAOImplTest.class, ClientDAOImplTest.class, PaymentDAOImplTest.class,
        BankAccountServiceTest.class
        , ClientServiceTest.class, PaymentServiceTest.class
})
public class JUnit5TestSuite {
}
