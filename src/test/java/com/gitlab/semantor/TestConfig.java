package com.gitlab.semantor;

import com.gitlab.semantor.restful.service.BankAccountService;
import com.gitlab.semantor.restful.service.ClientService;
import com.gitlab.semantor.restful.service.PaymentService;
import com.gitlab.semantor.restful.dao.*;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.hibernate.cfg.AvailableSettings;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("com.gitlab.semantor.restful.dao")
@PropertySource("classpath:application-local.properties")
public class TestConfig {
    @Bean
    ClientDAOImpl clientDAO() {
        return new ClientDAOImpl(sessionFactoryTest().getObject());
    }

    @Bean
    BankAccountDAOImpl bankAccountDAO() {
        return new BankAccountDAOImpl(sessionFactoryTest().getObject());
    }

    @Bean
    PaymentDAOImpl paymentDAO() {
        return new PaymentDAOImpl(sessionFactoryTest().getObject());
    }

    @Bean
    BankAccountService bankAccountServiceWithMockDAO() {
        return new BankAccountService(Mockito.mock(BankAccountDAO.class),
                Mockito.mock(PaymentDAO.class),
                Mockito.mock(ClientDAO.class));
    }

    @Bean
    ClientService clientServiceWithMockDAO() {
        return new ClientService(Mockito.mock(ClientDAO.class),
                Mockito.mock(BankAccountDAO.class),
                Mockito.mock(PaymentDAO.class));
    }

    @Bean
    PaymentService paymentServiceWithMockDAO() {
        return new PaymentService(Mockito.mock(PaymentDAO.class), Mockito.mock(BankAccountDAO.class));
    }

    @Autowired
    private Environment environment;

    @Bean
    public DataSource getDataSourceTest() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
        dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
        dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
        dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
        return dataSource;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactoryTest() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(getDataSourceTest());
        sessionFactory.setPackagesToScan("com.gitlab.semantor");
        sessionFactory.setHibernateProperties(getHibernateProperties());
        return sessionFactory;
    }

    @Bean
    public HibernateTransactionManager transactionManagerTest() {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactoryTest().getObject());
        return transactionManager;
    }


    private Properties getHibernateProperties() {
        Properties properties = new Properties();
        properties.put(AvailableSettings.DIALECT, environment.getRequiredProperty("hibernate.dialect"));
        properties.put(AvailableSettings.SHOW_SQL, environment.getRequiredProperty("hibernate.show_sql"));
//        properties.put(AvailableSettings.STATEMENT_BATCH_SIZE, env.getRequiredProperty("hibernate.batch.size"));
//        properties.put(AvailableSettings.HBM2DDL_AUTO, env.getRequiredProperty("hibernate.hbm2ddl.auto"));
//        properties.put(AvailableSettings.CURRENT_SESSION_CONTEXT_CLASS, env.getRequiredProperty("hibernate.current.session.context.class"));
        return properties;
    }
}