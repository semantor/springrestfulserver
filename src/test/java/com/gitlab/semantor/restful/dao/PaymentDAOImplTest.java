package com.gitlab.semantor.restful.dao;

import com.gitlab.semantor.TestConfig;
import com.gitlab.semantor.restful.entity.BankAccount;
import com.gitlab.semantor.restful.entity.Client;
import com.gitlab.semantor.restful.entity.Payment;
import com.gitlab.semantor.restful.entity.Status;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Date;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
@Sql({"classpath:schema.sql"})
public class PaymentDAOImplTest {
    public static final Client firstClient = new Client(1, "Vasiliy", "Zaycev", Date.valueOf("1915-03-23"));
    public static final BankAccount bankAccount = new BankAccount(1, 1234457812344575L, 250000, firstClient);

    @Autowired
    PaymentDAO paymentDAO;

    @Test
    void add() {
        Payment payment = new Payment(150, bankAccount);
        assertEquals(payment, paymentDAO.add(payment).get());
    }

    @Test
    void getAll() {
        paymentDAO.getAll();
    }

    @Test
    void get() {
        Payment payment = new Payment(1, bankAccount, 100, Date.valueOf(LocalDate.now()), Status.NOT_STARTED.getStatus());
        assertEquals(payment, paymentDAO.get(1).get());
    }

    @Test
    void update() {
        Payment payment = new Payment(2, bankAccount, 450, Date.valueOf(LocalDate.now()), Status.NOT_STARTED.getStatus());
        assertEquals(payment, paymentDAO.update(2, payment).get());
    }

    @Test
    void delete() {
        paymentDAO.delete(3);
        assertTrue(paymentDAO.get(3).isEmpty());
    }

    @Test
    void getClientPayments() {
        BankAccount bankAccount = new BankAccount(2384728934793273L, 300000);
        Payment payment = new Payment(6, bankAccount, 233, Date.valueOf(LocalDate.now()), Status.NOT_STARTED.getStatus());
        assertEquals(payment, paymentDAO.getClientPayments(2).get(0));
    }

    @Test
    void getBankAccountPayments() {
        BankAccount bankAccount = new BankAccount(1234412312344575L, 100000);
        Payment expected = new Payment(4, bankAccount, 450, Date.valueOf(LocalDate.now()), Status.NOT_STARTED.getStatus());
        assertEquals(expected, paymentDAO.getBankAccountPayments(2).get(0));
    }

    @Test
    void setStatus(){
        paymentDAO.setStatus(5,Status.PROCESSING);
        assertEquals(Status.PROCESSING.getStatus(), paymentDAO.get(5).get().getStatus());
    }

}