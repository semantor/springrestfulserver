package com.gitlab.semantor.restful.dao;

import com.gitlab.semantor.TestConfig;
import com.gitlab.semantor.restful.entity.BankAccount;
import com.gitlab.semantor.restful.entity.Client;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
@Sql({"classpath:schema.sql"})
public class BankAccountDAOImplTest {
    public static final Client firstClient = new Client(1, "Vasiliy", "Zaycev", Date.valueOf("1915-03-23"));

    @Autowired
    BankAccountDAO bankAccountDAO;

    @Test
    void add() {
        BankAccount bankAccount = new BankAccount(4739_3452_3476_3461L, 205, firstClient);
        assertEquals(bankAccount, bankAccountDAO.add(bankAccount).get());
    }

    @Test
    void getAll() {
        bankAccountDAO.getAll();
    }

    @Test
    void get() {
        BankAccount client = new BankAccount(1, 1234457812344575L, 250000, firstClient);
        assertEquals(client, bankAccountDAO.get(client.getId()).get());
    }

    @Test
    void update() {
        BankAccount bankAccount = new BankAccount(2, 1234457812344575L, 100, firstClient);
        assertEquals(bankAccount, bankAccountDAO.update(2, bankAccount).get());
    }

    @Test
    void delete() {
        bankAccountDAO.delete(3);
        assertTrue(bankAccountDAO.get(3).isEmpty());
    }

    @Test
    void getClientBankAccounts() {
        BankAccount expected = new BankAccount(2384728934793273L, 300000);
        BankAccount  input = bankAccountDAO.getClientBankAccounts(2).get(0);
        assertEquals(expected, input);
    }


}