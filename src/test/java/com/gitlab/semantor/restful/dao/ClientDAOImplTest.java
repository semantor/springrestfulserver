package com.gitlab.semantor.restful.dao;

import com.gitlab.semantor.TestConfig;
import com.gitlab.semantor.restful.entity.Client;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
@Sql({"classpath:schema.sql"})
public class ClientDAOImplTest {

    @Autowired
    ClientDAO clientDAO;

    @Test
    void add() {
        Client client = new Client("Georgii", "Kirillov", Date.valueOf("1990-12-12"));
        assertEquals(client, clientDAO.add(client).get());
    }

    @Test
    void getAll() {

    }

    @Test
    void get() {
        Client client = new Client(1, "Vasiliy", "Zaycev", Date.valueOf("1915-03-23"));
        System.out.println(clientDAO.get(client.getId()).get());
        assertEquals(client, clientDAO.get(client.getId()).get());
    }

    @Test
    void update() {
        Client client = new Client(2, "Fedor", "Smolov", Date.valueOf("1990-02-09"));
        assertEquals(client, clientDAO.update(2, client).get());
    }

    @Test
    void delete() {
        clientDAO.delete(3);
        assertTrue(clientDAO.get(3).isEmpty());
    }
}