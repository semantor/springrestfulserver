package com.gitlab.semantor.restful.service;

import com.gitlab.semantor.TestConfig;
import com.gitlab.semantor.restful.dao.BankAccountDAO;
import com.gitlab.semantor.restful.dao.PaymentDAOImplTest;
import com.gitlab.semantor.restful.entity.BankAccount;
import com.gitlab.semantor.restful.entity.Client;
import com.gitlab.semantor.restful.entity.Payment;
import com.gitlab.semantor.restful.exceptions.DatabaseException;
import com.gitlab.semantor.restful.exceptions.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
@Sql({"classpath:schema.sql"})
public class BankAccountServiceTest {

    @Autowired
    @Qualifier("bankAccountServiceWithMockDAO")
    BankAccountService bankAccountService;
    private final BankAccount bankAccount = new BankAccount(45682862464L, 12332);

    @Test
    void getAllBankAccounts() {
        List<BankAccount> bankAccounts = new ArrayList<>();
        bankAccounts.add(bankAccount);
        BankAccountDAO bankAccountDAO = bankAccountService.getBankAccountDAO();
        Mockito.when(bankAccountDAO.getAll())
                .thenReturn(bankAccounts);

        assertEquals(bankAccounts, bankAccountService.getAllBankAccounts());
    }

    @Test
    void getBankAccountByID() {
        Mockito.when(bankAccountService.getBankAccountDAO().get(1)).thenReturn(Optional.of(bankAccount));

        assertEquals(bankAccount, bankAccountService.getBankAccountByID(1));
    }

    @Test
    void getBankAccountByIDNull() {
        Mockito.when(bankAccountService.getBankAccountDAO().get(1)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class,()-> bankAccountService.getBankAccountByID(1));
    }

    @Test
    void getPaymentByBankAccount() {
        Payment payment = new Payment(333, PaymentDAOImplTest.bankAccount);
        List<Payment> payments = new ArrayList<>();
        payments.add(payment);
        Mockito.when(bankAccountService.getPaymentDAO().getBankAccountPayments(2)).thenReturn(payments);

        assertEquals(payments, bankAccountService.getPaymentByBankAccount(2));
    }

    @Test
    void createNewBankAccountClientNotPresent() {
        Mockito.when(bankAccountService.getClientDAO().get(2)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> bankAccountService.createNewBankAccount(2));

    }

    @Test
    void createNewBankAccountWithDBError() {
        Mockito.when(bankAccountService.getClientDAO().get(2)).thenReturn(Optional.of(Client.defaultClient));
        Mockito.when(bankAccountService.getBankAccountDAO().add(bankAccount)).thenReturn(Optional.empty());

        assertThrows(DatabaseException.class,()-> bankAccountService.createNewBankAccount(2));
    }


//    @Test
//    void createNewBankAccountSuccessfully() {
//        Mockito.when(bankAccountService.getClientDAO().get(2)).thenReturn(Optional.of(Client.defaultClient));
//
//        Mockito.when(bankAccountService.generateNewBankAccountNumber()).thenReturn(45682862464L);
//
//        BankAccount bankAccount1 =  bankAccount;
//        bankAccount1.setClient(Client.defaultClient);
//        bankAccount1.setBalance(0);
//        Mockito.when(bankAccountService.getBankAccountDAO().add(bankAccount)).thenReturn(Optional.of(bankAccount));
//
//        assertEquals(Successfully, bankAccountService.createNewBankAccount(2));
//
//    }

    @Test
    void updateBankAccountWrongData() {
        Mockito.when(bankAccountService.getBankAccountDAO().get(4)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class,()-> bankAccountService.updateBankAccount(4, BankAccount.defaultBankAccount));
    }


    @Test
    void updateBankAccountSomeDBproblem() {
        BankAccount oldBA = new BankAccount(123412341234L, 152);
        BankAccount newBA = new BankAccount(456486284246L, 789);
        Mockito.when(bankAccountService.getBankAccountDAO().update(4, newBA)).thenReturn(Optional.empty());
        Mockito.when(bankAccountService.getBankAccountDAO().get(4)).thenReturn(Optional.of(oldBA));
        assertThrows(DatabaseException.class,()-> bankAccountService.updateBankAccount(4, newBA));
    }


    @Test
    void updateBankAccountSuccessfully() {
        BankAccount oldBA = new BankAccount(123412341234L, 152);
        BankAccount newBA = new BankAccount(456486284246L, 789);
        Mockito.when(bankAccountService.getBankAccountDAO().get(4)).thenReturn(Optional.of(oldBA));

        Mockito.when(bankAccountService.getBankAccountDAO().update(4, newBA)).thenReturn(Optional.of(newBA));
        assertEquals(newBA, bankAccountService.updateBankAccount(4, newBA));
    }

    @Test
    void deleteBankAccountWrongData() {
        BankAccount deletedBA = new BankAccount(123412341234L, 152);
        Mockito.when(bankAccountService.getBankAccountDAO().delete(4)).thenReturn(Optional.empty());
        assertThrows(IllegalArgumentException.class,()-> bankAccountService.deleteBankAccount(4));
    }

    @Test
    void deleteBankAccountWrong() {
        BankAccount deletedBA = new BankAccount(123412341234L, 152);
        Mockito.when(bankAccountService.getBankAccountDAO().delete(4)).thenReturn(Optional.of(deletedBA));
        assertEquals(deletedBA, bankAccountService.deleteBankAccount(4));
    }

    @Test
    void generateNewBankAccountNumber() {

    }
}