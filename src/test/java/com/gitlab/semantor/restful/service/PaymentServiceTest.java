package com.gitlab.semantor.restful.service;

import com.gitlab.semantor.TestConfig;
import com.gitlab.semantor.restful.entity.BankAccount;
import com.gitlab.semantor.restful.entity.Client;
import com.gitlab.semantor.restful.entity.Payment;
import com.gitlab.semantor.restful.entity.Status;
import com.gitlab.semantor.restful.exceptions.DatabaseException;
import com.gitlab.semantor.restful.exceptions.EntityNotFoundException;
import com.gitlab.semantor.restful.exceptions.NotEnoughDataException;
import com.gitlab.semantor.restful.exceptions.NotEnoughMoneyException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
@Sql({"classpath:schema.sql"})
public class PaymentServiceTest {

    @Autowired
    @Qualifier("paymentServiceWithMockDAO")
    PaymentService paymentService;

    private final Payment payment = new Payment(45628, new BankAccount(828724804984L, 45489));

    @Test
    void getAllPayments() {
        List<Payment> payments = new ArrayList<>();
        payments.add(payment);
        Mockito.when(paymentService.getPaymentDAO().getAll()).thenReturn(payments);

        assertEquals(payments, paymentService.getAllPayments());
    }

    @Test
    void getPaymentByIDWithError() {
        Mockito.when(paymentService.getPaymentDAO().get(6)).thenReturn(Optional.empty());

        assertEquals(Payment.defaultPayment, paymentService.getPaymentByID(6));

    }

    @Test
    void getPaymentByID() {
        Mockito.when(paymentService.getPaymentDAO().get(6)).thenReturn(Optional.of(payment));

        assertEquals(payment, paymentService.getPaymentByID(6));
    }

    @Test
    void createNewPaymentWithZeroPrice() {
        Payment payment = new Payment(0, new BankAccount(824892546l, 12312));

        assertThrows(NotEnoughDataException.class, () -> paymentService.createNewPayment(payment));
    }

    @Test
    void createNewPaymentWithZeroBankAccount() {
        Payment payment = new Payment(12210, new BankAccount(0, 12312));

        assertThrows(NotEnoughDataException.class, () -> paymentService.createNewPayment(payment));
    }

    @Test
    void createNewPaymentWrongDataIncome() {
        List<BankAccount> bankAccounts = new ArrayList<>();
        bankAccounts.add(new BankAccount(46546828L, 123));
        bankAccounts.add(new BankAccount(4862484001L, 486));
        bankAccounts.add(new BankAccount(8945646565L, 123));
        Mockito.when(paymentService.getBankAccountDAO().getAll()).thenReturn(bankAccounts);

        Payment payment = new Payment(12210, new BankAccount(68848640, 12312));

        assertThrows(EntityNotFoundException.class, () -> paymentService.createNewPayment(payment));
    }

    @Test
    void createNewPaymentNotEnoughMoney() {
        List<BankAccount> bankAccounts = new ArrayList<>();
        bankAccounts.add(new BankAccount(46546828L, 123));
        bankAccounts.add(new BankAccount(4862484001L, 486));
        bankAccounts.add(new BankAccount(68848640L, 123));
        Mockito.when(paymentService.getBankAccountDAO().getAll()).thenReturn(bankAccounts);

        Payment payment = new Payment(12210, new BankAccount(68848640L, 12312));

        assertThrows(NotEnoughMoneyException.class, () -> paymentService.createNewPayment(payment));
    }

    @Test
    void createNewPaymentSomeDBProblemWithAddingPayment() {
        Payment payment = new Payment(12, new BankAccount(68848640L, 12312));

        List<BankAccount> bankAccounts = new ArrayList<>();
        bankAccounts.add(new BankAccount(46546828L, 123));
        bankAccounts.add(new BankAccount(4862484001L, 486));
        bankAccounts.add(new BankAccount(68848640L, 123));
        Mockito.when(paymentService.getBankAccountDAO().getAll()).thenReturn(bankAccounts);
        Mockito.when(paymentService.getPaymentDAO().add(payment)).thenReturn(Optional.empty());

        assertThrows(DatabaseException.class, () -> paymentService.createNewPayment(payment));
    }

    @Test
    void createNewPaymentSomeDBProblemWithUpdatingBankAccount() {
        Payment payment = new Payment(5, new BankAccount(68848640L, 12312),
                12, Date.valueOf(LocalDate.now()), Status.NOT_STARTED.getStatus());

        List<BankAccount> bankAccounts = new ArrayList<>();
        bankAccounts.add(new BankAccount(46546828L, 123));
        bankAccounts.add(new BankAccount(4862484001L, 486));
        BankAccount b = new BankAccount(1, 68848640L, 123, Client.defaultClient);
        bankAccounts.add(b);
        Mockito.when(paymentService.getBankAccountDAO().getAll()).thenReturn(bankAccounts);
        Mockito.when(paymentService.getPaymentDAO().add(payment)).thenReturn(Optional.of(payment));
        b.setBalance(b.getBalance() - payment.getPrice());
        Mockito.when(paymentService.getBankAccountDAO().update(b.getId(), b)).thenReturn(Optional.empty());
        Mockito.when(paymentService.getPaymentDAO().delete(payment.getId())).thenReturn(Optional.of(payment));

        assertThrows(DatabaseException.class, () -> paymentService.createNewPayment(payment));
    }

    @Test
    void createNewPaymentSuccessfully() {
        Payment payment = new Payment(5, new BankAccount(68848640L, 12312),
                12, Date.valueOf(LocalDate.now()), Status.NOT_STARTED.getStatus());

        List<BankAccount> bankAccounts = new ArrayList<>();
        bankAccounts.add(new BankAccount(46546828L, 123));
        bankAccounts.add(new BankAccount(4862484001L, 486));
        BankAccount b = new BankAccount(1, 68848640L, 123, Client.defaultClient);
        bankAccounts.add(b);
        Mockito.when(paymentService.getBankAccountDAO().getAll()).thenReturn(bankAccounts);
        Mockito.when(paymentService.getPaymentDAO().add(payment)).thenReturn(Optional.of(payment));
        b.setBalance(b.getBalance() - payment.getPrice());
        Mockito.when(paymentService.getBankAccountDAO().update(b.getId(), b)).thenReturn(Optional.of(b));

        assertEquals(payment, paymentService.createNewPayment(payment));
    }

    @Test
    void cancelPaymentWrongDataIncome() {
        Mockito.when(paymentService.getPaymentDAO().delete(3)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> paymentService.cancelPayment(3));
    }

    @Test
    void cancelPaymentSuccessfully() {
        Payment payment = new Payment(5, new BankAccount(68848640L, 12312),
                12, Date.valueOf(LocalDate.now()), Status.NOT_STARTED.getStatus());

        Mockito.when(paymentService.getPaymentDAO().delete(3)).thenReturn(Optional.of(payment));

        assertEquals(payment, paymentService.cancelPayment(3));
    }

}