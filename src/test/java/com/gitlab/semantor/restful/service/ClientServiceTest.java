package com.gitlab.semantor.restful.service;

import com.gitlab.semantor.TestConfig;
import com.gitlab.semantor.restful.entity.BankAccount;
import com.gitlab.semantor.restful.entity.Client;
import com.gitlab.semantor.restful.entity.Payment;
import com.gitlab.semantor.restful.exceptions.AlreadyExistException;
import com.gitlab.semantor.restful.exceptions.DatabaseException;
import com.gitlab.semantor.restful.exceptions.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
@Sql({"classpath:schema.sql"})
public class ClientServiceTest {

    @Autowired
    @Qualifier("clientServiceWithMockDAO")
    ClientService clientService;

    @Test
    void getAllClient() {
        List<Client> clients = new ArrayList<>();
        clients.add(new Client("qwe", "asdf", Date.valueOf(LocalDate.now())));
        Mockito.when(clientService.getClientDAO().getAll()).thenReturn(clients);

        assertEquals(clients, clientService.getAllClient());
    }

    @Test
    void getClientByID() {
        Client client = new Client("qwe", "asdf", Date.valueOf(LocalDate.now()));
        Mockito.when(clientService.getClientDAO().get(1)).thenReturn(Optional.of(client));

        assertEquals(client, clientService.getClientByID(1));

    }

    @Test
    void getClientByIDWithDBProblem() {
        Client client = new Client("qwe", "asdf", Date.valueOf(LocalDate.now()));
        Mockito.when(clientService.getClientDAO().get(1)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class,()-> clientService.getClientByID(1));

    }

    @Test
    void getBankAccountByClient() {
        List<BankAccount> bankAccounts = new ArrayList<>();
        bankAccounts.add(new BankAccount(498262465465L, 123123));
        Mockito.when(clientService.getBankAccountDAO().getClientBankAccounts(1)).thenReturn(bankAccounts);

        assertEquals(bankAccounts, clientService.getBankAccountByClient(1));
    }

    @Test
    void getClientPayments() {
        List<Payment> payments = new ArrayList<>();
        payments.add(new Payment(12323, new BankAccount(498262465465L, 123123)));
        Mockito.when(clientService.getPaymentDAO().getClientPayments(3)).thenReturn(payments);

        assertEquals(payments, clientService.getClientPayments(3));
    }


    @Test
    void createNewClientAlreadyExist() {
        Client newClient = new Client("simpleName", "simpleSurname", Date.valueOf("1990-10-21"));
        List<Client> clientsWithNewClient = new ArrayList<>();
        clientsWithNewClient.add(newClient);
        Mockito.when(clientService.getAllClient()).thenReturn(clientsWithNewClient);
        Mockito.when(clientService.getClientDAO().add(newClient)).thenReturn(Optional.of(newClient));

        assertThrows(AlreadyExistException.class, () -> clientService.createNewClient(newClient));
    }

    @Test
    void createNewClientSomeDbProblem() {
        Client newClient = new Client("simpleName", "simpleSurname", Date.valueOf("1990-10-21"));
        List<Client> clientsWithNewClient = new ArrayList<>();
        Mockito.when(clientService.getAllClient()).thenReturn(clientsWithNewClient);
        Mockito.when(clientService.getClientDAO().add(newClient)).thenReturn(Optional.of(newClient));
        Mockito.when(clientService.getClientDAO().add(newClient)).thenReturn(Optional.empty());

        assertThrows(DatabaseException.class, () -> clientService.createNewClient(newClient));
    }

    @Test
    void createNewClientSuccessfully() {
        Client newClient = new Client("simpleName", "simpleSurname", Date.valueOf("1990-10-21"));
        List<Client> clientsWithNewClient = new ArrayList<>();
        Mockito.when(clientService.getAllClient()).thenReturn(clientsWithNewClient);
        Mockito.when(clientService.getClientDAO().add(newClient)).thenReturn(Optional.of(newClient));
        Mockito.when(clientService.getClientDAO().add(newClient)).thenReturn(Optional.of(newClient));

        assertEquals(newClient, clientService.createNewClient(newClient));
    }

    @Test
    void updateClientByIDWrongDataIncome() {
        Client newClient = new Client("simpleName", "simpleSurname", Date.valueOf("1990-10-21"));
        Mockito.when(clientService.getClientDAO().get(4)).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> clientService.updateClientByID(4, newClient));
    }


    @Test
    void updateClientByIDSomeDBProblem() {
        Client newClient = new Client("simpleName", "simpleSurname", Date.valueOf("1990-10-21"));
        Client oldClient = new Client("oldName", "oldSurname", Date.valueOf("1989-10-13"));
        Mockito.when(clientService.getClientDAO().get(4)).thenReturn(Optional.of(oldClient));
        Mockito.when(clientService.getClientDAO().update(4, newClient)).thenReturn(Optional.empty());

        assertThrows(DatabaseException.class, () -> clientService.updateClientByID(4, newClient));
    }

    @Test
    void updateClientByIDSuccessfully() {
        Client newClient = new Client("simpleName", "simpleSurname", Date.valueOf("1990-10-21"));
        Client oldClient = new Client("oldName", "oldSurname", Date.valueOf("1989-10-13"));
        Mockito.when(clientService.getClientDAO().get(4)).thenReturn(Optional.of(oldClient));
        Mockito.when(clientService.getClientDAO().update(4, newClient)).thenReturn(Optional.of(newClient));

        assertEquals(newClient, clientService.updateClientByID(4, newClient));
    }

    @Test
    void deleteClientByIDWrongDataIncome() {
        Mockito.when(clientService.getClientDAO().delete(4)).thenReturn(Optional.empty());
        assertThrows(IllegalArgumentException.class, () -> clientService.deleteClientByID(4));
    }

    @Test
    void deleteClientByIDSuccessfully() {
        Client deletedClient = new Client("simpleName", "simpleSurname", Date.valueOf("1990-10-21"));

        Mockito.when(clientService.getClientDAO().delete(4)).thenReturn(Optional.of(deletedClient));
        assertEquals(deletedClient, clientService.deleteClientByID(4));
    }

}